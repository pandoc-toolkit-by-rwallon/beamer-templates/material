###############################################################################
#      MAKEFILE FOR EASILY BUILDING A MATERIAL PRESENTATION WITH PANDOC       #
###############################################################################

##########
# SOURCE #
##########

# The location of the Material template.
MATERIAL_HOME = .

# The name of the file containing the metadata of the presentation.
# It may be left blank, e.g., if these data are put in the Markdown source file.
METADATA =

# The name of the Markdown source file to build the presentation from.
# Do not put the extension of this file.
# For example, if this file is 'example.md', FILENAME must be set to 'example'.
FILENAME =

# The name of the Markdown source file for the backup slides.
# Leave empty if you do not have backup slides.
# Do not put the extension of this file.
# For example, if this file is 'backup.md', BACKUP must be set to 'backup'.
BACKUP =

# Where to find LaTeX packages, including Material.
# This variable should be left as-is, unless you know what you are doing!
TEXINPUTS = $(MATERIAL_HOME)/sty:$(shell kpsewhich --expand-var='$$TEXINPUTS')

###################
# CUSTOM SETTINGS #
###################

# The title level corresponding to the title of the slides.
SLIDE_LEVEL = 1

# Whether talk highlights must be used as a conclusion.
HIGHLIGHTS =

########################
# COMPILATION SETTINGS #
########################

# The options to pass to Pandoc when converting from Markdown to LaTeX.
# See Pandoc's documentation for more details.
PANDOC_OPTS =

# The engine used to produce the PDF from the LaTeX source.
# Replace it by 'pdflatex' if you do not have 'xelatex'.
PDF_ENGINE = xelatex

# Options for the LaTeX compilation.
TEXOPTIONS = -interaction=nonstopmode

# All the files required to build the presentation.
ALL_REQUIREMENTS = $(FILENAME).tex
HIGHLIGHTS_REQUIREMENTS = $(FILENAME)-highlights.tex

# If there are backup slides, they must also be converted.
ifdef BACKUP
    ALL_REQUIREMENTS += $(BACKUP).tex
    HIGHLIGHTS_REQUIREMENTS += $(BACKUP).tex
    PANDOC_OPTS += -V backup:$(BACKUP)
endif

# If there are talk highlights, we need to build a separate presentation.
ifdef HIGHLIGHTS
    ALL_REQUIREMENTS += $(FILENAME)-highlights.pdf
    PANDOC_OPTS += -V highlights:$(FILENAME)-highlights.pdf
endif

###########
# TARGETS #
###########

# Declares non-file targets.
.PHONY: clean mrproper help

# Builds the presentation from the LaTeX source file.
$(FILENAME).pdf: $(ALL_REQUIREMENTS)
	@export TEXINPUTS=${TEXINPUTS}; $(PDF_ENGINE) $(TEXOPTIONS) $(FILENAME).tex
	bibtex $(FILENAME) || true
	@export TEXINPUTS=${TEXINPUTS}; $(PDF_ENGINE) $(TEXOPTIONS) $(FILENAME).tex
	@export TEXINPUTS=${TEXINPUTS}; $(PDF_ENGINE) $(TEXOPTIONS) $(FILENAME).tex

# Converts the main Markdown source file into LaTeX.
$(FILENAME).tex: $(MATERIAL_HOME)/material.pandoc $(METADATA) $(FILENAME).md
	pandoc $(PANDOC_OPTS) --to beamer --template $(MATERIAL_HOME)/material.pandoc --natbib --slide-level $(SLIDE_LEVEL) $(METADATA) $(FILENAME).md --output $(FILENAME).tex

# Converts the backup Markdown source file into LaTeX.
$(BACKUP).tex: $(BACKUP).md
	pandoc $(PANDOC_OPTS) --to beamer --natbib --slide-level $(SLIDE_LEVEL) $(BACKUP).md --output $(BACKUP).tex

# Builds the (temporary) presentation to be used for talk highlights.
$(FILENAME)-highlights.pdf: $(HIGHLIGHTS_REQUIREMENTS)
	@export TEXINPUTS=${TEXINPUTS}; $(PDF_ENGINE) $(TEXOPTIONS) $(FILENAME)-highlights.tex
	bibtex $(FILENAME)-highlights || true
	@export TEXINPUTS=${TEXINPUTS}; $(PDF_ENGINE) $(TEXOPTIONS) $(FILENAME)-highlights.tex
	@export TEXINPUTS=${TEXINPUTS}; $(PDF_ENGINE) $(TEXOPTIONS) $(FILENAME)-highlights.tex

# Converts the main Markdown source file into LaTeX for talk highlights.
$(FILENAME)-highlights.tex: $(MATERIAL_HOME)/material.pandoc $(METADATA) $(FILENAME).md
	pandoc $(PANDOC_OPTS) -V no-highlights:true --to beamer --template $(MATERIAL_HOME)/material.pandoc --natbib --slide-level $(SLIDE_LEVEL) $(METADATA) $(FILENAME).md --output $(FILENAME)-highlights.tex

# Removes the auxiliary files used to build the presentation.
clean:
	rm -rf *.aux *.bbl *.blg *.log *.nav *.out *.snm *.tex *.toc *.vrb
	rm -rf *-highlights.pdf

# Removes all generated files, including the presentation.
mrproper: clean
	rm -rf $(FILENAME).pdf

# Prints a message describing useful targets.
help:
	@echo
	@echo "Build your Material Presentation with Pandoc"
	@echo "============================================"
	@echo
	@echo "You may want to use one of the following targets:"
	@echo "    - $(FILENAME).pdf: builds the presentation (default)"
	@echo "    - clean: removes auxiliary files"
	@echo "    - mrproper: removes all generated files"
	@echo "    - help: displays this help"
	@echo
