# Changelog

This file describes the evolution of the *Material* template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.2.0 (December 2021)

+ Font size can now be set from YAML.
+ Slide size can now be set from YAML.
+ Style for overlays can now be set from YAML.
+ Structure color can now be set from YAML.
+ Block colors can now be set from YAML.
+ Talk highlights, showing miniatures of some slides, can be used to sum up
  the presentation.
+ Backup slides can be built from a Markdown file, and appended to the
  presentation after the closing slide.
+ Bibliography references can be cited with `pandoc-citeproc`.
+ Building a presentation no longer requires to copy all the template's files
  in the same directory as the presentation.

## Version 0.1.0 (October 2019)

+ LaTeX preamble can be customized from YAML.
+ Colors and fonts can be modified from YAML.
+ Template-specific Pandoc integrations can be turned off from YAML.
+ Document metadata can be set from YAML.
+ Institute logo(s) can be put on the title slide by specifying them in YAML.
+ Builds can be easily customized by setting `make` variables from the command
  line.
