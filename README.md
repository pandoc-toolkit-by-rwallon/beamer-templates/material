# Pandoc Template for the *Material* Beamer Theme

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/material/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/material/commits/master)

## Description

This project provides a template allowing an easy customization of the
[*Material* Beamer Theme](https://github.com/llancia/MaterialBeamer) for a
smooth integration with [Pandoc](https://pandoc.org).

This template, and in particular the bundled [`sty`](./sty) files of
*Material*, is distributed under the GNU General Public License, version 3.

## Requirements

To build your presentation using this template, [Pandoc](https://pandoc.org)
at least 2.0 and [LaTeX](https://www.latex-project.org/) have to be installed
on your computer.

*Material* (as-is from [this](https://github.com/llancia/MaterialBeamer/commit/74c7629)
commit) is bundled with this template, so you are not required to install it.

To get the best from *Material*, you will finally need XeLaTeX, which should be
bundled with your LaTeX distribution.

## Creating your Presentation

This template enables to write amazing *Material* presentations in plain
[Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown), thanks to the
power of Pandoc.

To create your presentation, you will need to put the `sty` directory, the
`Makefile` and the template `material.pandoc` inside the directory in
which you have your Markdown source file and the (optional) metadata file.

The template we provide supports many customizations, so that you can get the
best of *Material* without having to write LaTeX code.

You will find below a description of the available customizations.
You may also be interested in our [examples](./examples), which illustrate
various use cases.

### About the Title Slide

First, note that our template does not use *exactly* the title slide defined by
*Material*.
In particular, the logos are put in a table at the top of the slide, so as to
make easier the use of multiple of them.

Moreover, this slide is also used as *closing* slide, as commonly advised
for presentations.

### YAML Configuration

Most customizations are set in the YAML configuration.
You are free to put this configuration in a separate file, or at the top of
your Markdown file, provided that you set the variable `METADATA` accordingly
when building.

For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.
Read below for details on how to configure your presentation, and see our
various [examples](./examples) for different use cases.

> *Nota-Bene*
>
> Some variables used in the configuration are used as *Boolean variables*.
> However, Pandoc does not allow to *check* the actual content of a variable.
> As a consequence, we can only check whether a variable exists or not.
>
> This means that, if a variable is set to `false`, it will still be considered
> to be set, and it will not have the expected effect.
>
> That is why you must consider such variables as *flags*, that you set only
> if you need them, preferably to `true` for more readability.
>
> In the following, the description of flag variables always has the form
> "if present, ...".

#### Base Preamble Settings

Some of the base settings can be set with the following variables:

+ `handout`: if present, produces slides without pauses.
+ `language`: sets the language to be used by `babel` (default is `english`).
+ `package`: the list of packages to include in the presentation.
+ `include-before`: the list of all other LaTeX commands you want to add to the
  preamble.

#### Color Settings

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

Once you have defined your own colors, you may set the following properties
to customize the colors of your presentation (of course, you may also use
predefined colors):

+ `dark-color`: the dark color to use as background in the slides.
+ `light-color`: the light color to use as background in the slides.
+ `title-color`: the color for the titles of the presentation.
+ `main-color`: the color for the text of the slides.
+ `alert-color`: the color for alerted text.
+ `box-color`: the background color for boxes, which are used for *punchlines*.

#### Personal Settings

To write your *own* presentation, you need to set the following variables:

+ `title`: the title of your presentation.
+ `short-title`: the short version of the title of your presentation
  (optional).
+ `subtitle`: the subtitle of your presentation (optional).
+ `author`: the name of the author of the presentation (use a list if there is
  more than one author).
+ `short-author`: the short version of the name of the author of the
  presentation (use a list if there is more than one author).
+ `institute`: the name of the author's affiliation (use a list if there is
  more than one affiliation).
+ `event`: the event in which the presentation takes place (optional).
+ `date`: the date on which the presentation takes place (optional, default is
  the current date).

You may also want to add the logo(s) of your affiliation(s) to your title
slide.
This can be achieved by setting `logo` to the list of the images (given by
their path) to use as logos.
If the images are too big, you may also set `logo-height` to change their
height.
The slide width is proportionally divided between each logo.

#### Pandoc Integration

As Markdown does not offer as many features as LaTeX, our template redefines
some styling of *Material*.

First, any **bold** text will actually be interpreted as **alerted** text.
You may restore the original behavior by putting `bold: true` in the metadata.

Second, **quotes** are interpreted as **punchlines**, that is highlighted
blocks of text used as *plot twists* or *take-away* messages.
Once again, you may restore the original behavior by setting `quote: true` in
the metadata.

Last but not least, if you want to add an overview (table of contents) of your
presentation on your second slide, put `toc: true` in the metadata.

### Building your Presentation

Presentations based on this template are built using `make`.
To customize the build to your own use case, you may either update the
[`Makefile`](./Makefile) provided in this project, or set the needed variables
while building.

For example, suppose that your Markdown source file is `example.md`, your
metadata file is `metadata.md` and the titles of your slides correspond to
level-3 titles in your Markdown source file.
You will then have to type the following command to build your presentation:

```bash
make METADATA=metadata.md FILENAME=example SLIDE_LEVEL=3
```

Note that you may also set the program to use to create the PDF from LaTeX
source with `PDF_ENGINE` and customize the way Pandoc produces the slides by
setting the variable `PANDOC_OPTS` accordingly.
For more details, read [Pandoc's User Guide](https://pandoc.org/MANUAL.html).

Our `Makefile` also provides `make clean`, which removes all generated files
but the PDF of the slides, and `make mrproper`, which also removes this PDF.

Type `make help` to have the full list of available targets.
