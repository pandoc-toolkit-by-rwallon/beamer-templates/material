---
# Personal Settings
title: Pandoc Template for Material
short-title: Material Template
subtitle: A Simple Slide Template for Latex Beamer
author: Romain Wallon
short-author: R. Wallon
institute: Pandoc Toolkit

# Event Information
event: Release 0.1.0
date: October 2019

# Logos
logo:
    - logos/markdown.png
    - logos/latex.png
logo-height: 1.4cm
---

# Introduction

## From Material to Pandoc

### Material

The **Material** Beamer theme is a simple slide template for LaTeX Beamer by
Lorenzo Lancia.

### Pandoc

**Pandoc** is a Haskell library for converting from one markup format to
another, and a command-line tool that uses this library.

### Why a Pandoc Template for Material?

To use *MaterialBeamer* with Pandoc-based presentations, you may run the
following command:

```bash
$ pandoc -t beamer -V theme:materialbeamer \
         -o output.pdf input.md
```

However, this often requires to write `LaTeX` code if you want to customize
your presentation.

We propose instead to design a highly-customizable Pandoc template for
*MaterialBeamer*.

# Writing your Presentation

## Elements

### Typography

Because Markdown is limited in terms of typography, you cannot combine bold and
alert fonts:

```markdown
The theme provides sensible defaults to
*emphasize* text, and to show **strong**
results.
```

becomes

The theme provides sensible defaults to *emphasize* text, and to show
**strong** results.

### Lists

There is a support for items:

+ Milk
+ Eggs
+ Potatoes

Enumerations are also supported:

1. First,
2. Second and
3. Last.

### Animations

Unfortunately, Markdown does not support Beamer animations...

\pause

But you can still write \LaTeX!

\pause

Simply put a `\pause`, and it will work perfectly!

### Figures

![This presentation is written in Mardown.](logos/markdown.png)

### Tables

Tables are supported through Pandoc's Markdown.

The largest cities in the world (source: Wikipedia) are:

| City         | Population  |
|--------------|-------------|
| Mexico City  | 20,116,842  |
| Shanghai     | 19,210,000  |
| Peking       | 15,796,450  |
| Istanbul     | 14,160,467  |

### Punchlines

*Punchlines* are implemented through the `quote` environment, unless you
specified that you did not want them:

```markdown
> Isn't it great?
```

They are used for *plot-twists* in your presentations, or for *take-away
messages*.

\pause

> Isn't it great?

# Conclusion

### Conclusion

Get the source of the theme on [GitHub](https://github.com/llancia/MaterialBeamer).

Get the source of the template and all the examples on
[GitLab](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/material).

> Both the theme and the template are licensed under the GNU General Public
> License, version 3.
