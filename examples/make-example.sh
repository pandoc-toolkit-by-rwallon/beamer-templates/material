#!/bin/bash
#
# Builds examples of the Pandoc Template for Material.
#
# $1 - The name of the metadata file containing the configuration of the
#      example to build.

#############
# FUNCTIONS #
#############

# Builds the specified example.
#
# Arguments:
#     $1 - The name of the example to build.
#
# Returns:
#     None
function build {
    cd tmp || return
    ln -s ../../Makefile ../../sty ../../material.pandoc ../logos ../example.md "../$1.md" .
    make METADATA="$1.md" FILENAME=example SLIDE_LEVEL=3
    mv example.pdf "../$1.pdf"
}

########
# MAIN #
########

# Checking the command line arguments.
test $# -ne 1 && echo "Usage: $0 <metadata-file>" && exit 1
test ! -f "$1" && echo "$0: $1: No such file or directory" && exit 2

# Getting the name of the example to build.
example=$(basename "$1" ".md")

# Building the example.
mkdir -p tmp
(build "$example")
rm -rf tmp

# Checking whether the build succeeded.
test ! -e "$example.pdf" && exit 3
exit 0
