---
# Color Definitions
color:
    - name: mylightblue
      type: RGB
      value: 142,246,237
    - name: mypurple
      type: RGB
      value: 152,45,240
    - name: mybluegreen
      type: RGB
      value: 15,109,156
    - name: mydarkblue
      type: RGB
      value: 57,36,126

# Color Settings
dark-color: mydarkblue
light-color: mybluegreen
title-color: mylightblue
main-color: mydarkblue
alert-color: mypurple
box-color: mybluegreen

# Pandoc Integration
toc: true
---
