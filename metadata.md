---
# Base Preamble Settings
handout:
language:
package:
    -
header-includes:
    -
include-before:
    -

# Color Definitions
color:
    - name:
      type:
      value:

# Color Settings
dark-color:
light-color:
title-color:
main-color:
alert-color:
box-color:

# Font Settings
sans-font:
mono-font:

# Pandoc Integration
bold:
quote:
toc:
links-as-notes:

# Personal Settings
title:
short-title:
subtitle:
author:
    -
short-author:
    -
institute:
    -

# Event Information
event:
date:

# Logos
logo:
    -
logo-height:

# Final Highlights
highlights-title:
highlights-width:
highlights-rows:
    - frames:
        -

# Bibliography
biblio-style:
natbiboptions:
    -
bibliography:
    -
---
